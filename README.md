This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

**You’ll need to have Node 8.16.0 or Node 10.16.0 or later version on your local machine.**

## Installation

To get the project up and running in the browser, complete the following steps:

1. Download and install Node: [https://nodejs.org/](https://nodejs.org/)
2. Using git, clone this repo: `git clone git@gitlab.com:starnavi-team/test-task-qa-v2.git` (SSH) or `git clone https://gitlab.com/starnavi-team/test-task-qa-v2.git` (HTTPS)
3. Swich to `develop` branch
4. Using Command line interface (CLI) go to the project directory
5. Install project dependancies: run `npm install` or `yarn install`
6. Start the development environment: run `npm start` or `yarn start`
7. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

